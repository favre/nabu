# About Nabu

Nabu is a tomography processing software being developed at [ESRF](https://www.esrf.eu) by the Data Analysis Unit. It is part of the [new ESRF tomography software suite](https://tomotools.gitlab-pages.esrf.fr/tomo-bridge-docs/data-proc/projects).

## Why Nabu

The European Synchrotron has several tomography beamlines. Each of them use dedicated software, which over the years led a variety of different tools spread over the beamlines with poor maintainability. This is summarized in [ESRF current situation for tomography software](https://tomotools.gitlab-pages.esrf.fr/tomo-bridge-docs/data-proc/situation/#current-situation-limitations-and-opportunities).

Nabu is an effort to unify tomography software in a new toolkit with the following requirements:

  - Library of tomography processing, with "applications" built on top of it, usable by both non-experts and power-users
  - High performance processing (parallelization with Cuda/OpenCL, computations distribution, memory re-use)
  - Support of [multiple techniques](https://tomotools.gitlab-pages.esrf.fr/tomo-bridge-docs/#tomography-techniques-at-esrf), not only absorption and phase contrast
  - Extensively documented
  - Focus on maintainability with a [bus factor](https://en.wikipedia.org/wiki/Bus_factor) greater than one
  - Compatible with ESRF legacy software, progressively replacing it

Nabu does not aim at being the new universal tomography reconstruction software. Well-established software like [Astra](http://www.astra-toolbox.com), [tomopy](http://tomopy.readthedocs.io), [Savu](https://github.com/DiamondLightSource/Savu) and [UFO](https://github.com/ufo-kit) have an extensive set of features. Nabu foremost focuses on ESRF needs, while being designed so that it can be re-used in other projects.

## Project management

### Development spot

The various projects are hosted on [gitlab.esrf.fr](https://gitlab.esrf.fr/tomotools).

### Members and meeting minutes

The [new ESRF tomography software suite](https://tomotools.gitlab-pages.esrf.fr/tomo-bridge-docs/data-proc/projects) is developed by ESRF Data Analysis Unit.  
The weekly meeting minutes [are available here](https://gitlab.esrf.fr/tomotools/minutes/-/tree/master).

### Roadmap

[Roadmap for Nabu developments](https://tomotools.gitlab-pages.esrf.fr/tomo-bridge-docs/data-proc/roadmap)


