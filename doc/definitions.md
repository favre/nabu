# Chunk processing

A tomography scan produces a collection of X-ray images. These can be of two types:
- Radios: raw intensity data acquired on the detector
- Flats, darks: for flat-field normalization

A **radiograph** (or **radio**) is the raw data at the output of a X-ray detector. 

## Radios and sinograms

In this documentation, we distinguish between radios and sinograms.  

As stated before, a radio is a raw projection data, i.e what comes out of the detector. "Corrected radios" refer to radios after some CCD corrections.

Each radio has *Nx* pixels horizontally, and *Nz* pixels vertically. During a scan, a collections of *Na* radios are acquired, where *Na* is the number of projection angles.

![radios](images/radios_stack.png)

In short, there are *Na* radios of *(Nx, Nz)* pixels.

In parallel geometry with circular trajectory, the sinogram 0 is obtained by extracting the first line of each radio. The sinogram number *z* is obtained by extracting the line *z* of each radio.

![sinos](images/sinos.png)

## Radios chunks

In Nabu, the radios are processed by chunks. A **chunk** is a collection of radios subregion: this is a fixed subregion taken in all the *Na* radios.

In the simplest setting, the first chunk (chunk zero) is obtained by reading the first *C* lines of each radio, i.e lines [0, *C*[. The second chunk (chunk 1) is obtained by reading lines [*C*, 2*C*[ ; and so on.  
Chunks are actually read with a slight overlap (see next figure).

Processing radios by chunks enables to process sinograms by batches, as **a radios chunk of size *C* gives a stack of *C* sinograms (or slices)** once the chunk is "transposed".


![chunks with overlap](images/chunks.png)

## Processing done in the radios space

![Radios processing](images/ccd_processing.png)

The process can also be started from any point. For example, we can start from phase-retrieved radios, by deactivating steps 1-2-3. 

## Processing done in the sinogram space

![Sinograms processing](images/sinos_processing.png)

The input is usually a chunk of radios, and the data will be transposed to be accessed as a stack of sinograms.

