# Installation

This page explains how to install nabu in various situations.


## Installing in a python environment

If you are familiar with the Python programming language and a package manager (`pip`, `conda`), you can install nabu using

```bash
pip install "nabu[full]"
```

this will install the latest (stable) release.


To install the current development version:
```bash
pip install git+https://gitlab.esrf.fr/tomotools/nabu.git
```

Note that nabu does not ship compiled code, so installing nabu itself should not be complicated ("pure-python package").

The difficult part is usually to install the optional dependencies (Cuda/OpenCL for JIT compilation of GPU code).
Notably, dependencies like pycuda and pycudwt do not have pre-compiled binaries, and the compilation will fail if your system does not have the Cuda toolkit installed.

We provide [pre-compiled wheels in the nabu wheelhouse](https://www.silx.org/pub/nabu/wheels).
Note that they were build for python 3.11, cuda 11.7 on architecture `SM_80`.
To use these wheels, you can use

```bash
pip install "nabu[full]" --find-links http://www.silx.org/pub/nabu/wheels --prefer-binary --trusted-host www.silx.org
```


## Installing using conda

Nabu does not have a conda recipe yet.
This section will show how to create a conda environment, and then to install nabu in this conda environment.

We recommend using [mamba](https://github.com/mamba-org/mamba) for an overall faster process.

```bash
mamba create -y -n nabu pip python=3.11
mamba activate nabu
mamba install -y pip wheel
mamba install -y scipy matplotlib
mamba install -y scikit-image silx hdf5plugin
mamba install -y ipython notebook ipympl
mamba install -y pyqt
```

```{note}
If you don't use mamba, simply replace "mamba" with "conda" in the above lines
```

**If your system does not have the cuda toolkit installed**, use the following lines:

```bash
mamba install cudatoolkit-dev
mamba install pycuda
```

This will install the whole cuda toolkit (including compiler). We usually avoid that to use the one of the system and reduce the environment size.

Then, switch to `pip` to install the remaining things:

```bash
pip cache purge
pip install --upgrade setuptools wheel

pip install pycuda --trusted-host www.silx.org --find-links http://www.silx.org/pub/nabu/wheels --prefer-binary
pip install pyopencl --trusted-host www.silx.org --find-links http://www.silx.org/pub/nabu/wheels --prefer-binary
pip install pyvkfft --trusted-host www.silx.org --find-links http://www.silx.org/pub/nabu/wheels --prefer-binary
pip install git+https://github.com/lebedov/scikit-cuda

pip install "nabu[full]" --find-links http://www.silx.org/pub/nabu/wheels --prefer-binary --trusted-host www.silx.org
```



## Frequently asked questions

### Does nabu need a GPU ?

No. Nabu can reconstruct a full volume without needing a GPU.

That being said:
  - The overall process will likely be slow
  - The FBP steps still requires [pyopencl](https://pypi.org/project/pyopencl), and OpenCL ICD/implementation installed on the host

Technically speaking, the reference implementation for each individual step (flat-field, filtering, etc) is in Python/numpy.

That's why **a GPU is recommended (not required)** if you want to use nabu.

### Which OS/python version are supported ?

  - Nabu is only tested on Linux. As it is primarily designed for running on a compute cluster, **there is no plan to support Windows or MacOS**. Pull requests for fixes on these platforms will still be welcome.

  - Nabu supports Python >= 3.5, though at this point many of its dependencies will probably require python >= 3.6 or 3.7.


### What about conda ?

Currently there is no conda recipe for nabu. There might be in the future.


### What are nabu dependencies ?

The most complicated part, when it comes to installing nabu, is the installation of its optional dependencies:
  - Computations acceleration (GPU/manycore CPU): `pycuda`, `scikit-cuda`
  - Image processing and fitting utils: `scikit-image`
  - Rings artefact removal using Fourier-Wavelets method on Nvidia GPUs: `pycudwt`
  - Fast and memory-efficient Fourier transform in Cuda/OpenCL: `pyvkfft`


