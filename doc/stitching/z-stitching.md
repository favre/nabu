# z-stitching

z-stitching can be done over:

* projections (in this case we will speak of pre-processing stitching)
* reconstructed volumes (in this case we will speak about post-processing stitching)

At nabu level most of the code is shared for those two use-case. Diversity comes mostly from getting and interpretting metadata of the scans or of the volumes.

## overlaping

To process successfully a stitching frames must have an overlaping area

![real overlap](images/z_stitch_real_overlap.png)

## pre processing z-stitching (projections)

You can stitch together a z-serie in pre processing by using the `PreProcessZStitcher`.
It requires as input a set of Nxtomo.

### real life  example of z-stitching projections
![concreate example of z-stitching with projections](images/concreate_example_proj_z_stitching.png)

### reference used for projection stitching

![reference used for projections stitching](images/reference_frame_radios.png)

## post processing z-stitching (reconstructed volumes)

You can stitch together a z-serie in pre processing by using the `PostProcessZStitcher`.
It requires as input a set of (reconstructed) volumes.

### real life example of z-stitching volumes

![concreate example of z-stitching with volumes](images/concreate_example_vol_z_stitching.png)

### reference used for volume stitching

![reference used for volume stitching](images/reference_frame_volume.png)

## stitching using the GUI

A GUI exists to stitch volumes with a graphical user interface. It is embed with tomwer. See TODO: add link

## stitching from the command line interface

You can apply stitching from CLI by:

* creating a configuration file
* editing the configuration
* calling `nabu-stitching` with the configuration file

### creating a configuration file

this can be done from:
```bash
nabu-stitching-config [-h] [--stitching-type STITCHING_TYPE] [--level LEVEL] [--output OUTPUT]

usage: nabu-stitching-config [-h] [--stitching-type STITCHING_TYPE] [--level LEVEL] [--output OUTPUT]

Initialize a nabu configuration file

optional arguments:
  -h, --help            show this help message and exit
  --stitching-type STITCHING_TYPE
                        User can provide stitching type to filter some parameters. Must be in ('z-preproc', 'z-postproc').
  --level LEVEL         Level of options to embed in the configuration file. Can be 'required', 'optional', 'advanced'.
  --output OUTPUT       output file to store the configuration

```

### editing a configuration file

The different field of the configuration are explained within the file. But maybe some precision can be added regarding:

* input section
    * `slices`: You can apply the stitching on a sub-section only (for debug or distribution for example).
    This sub-section can be specify using the `slices` value from the configuration file:

    ```text
    [inputs]
    slices=
    ```

    `slices` can be provided as a slice like `slices=start:stop[:step]` or as a list of value like `slices=start` or `slices=1,20,55`. Giving them as slices (with step equal 1) is recommanded because it will allow to load contiguous data faster.


### calling `nabu-stitching` with the configuration file

Once you finished editing the stitching configuration file you can execute the stitching from:

```bash
nabu-stitching [my_stitching_file]
```

This will trigger the processing.
If this is a pre-processing stitching then this will generate a NXtomo that you will be able to reconstruct using the `nabu` command line and a nabu reconstruction file.
If this is a post-processing stitching then you will obtain a volume.

### from python and notebook

to ease processing stitching and get a better view of the behavior there is two notebooks that could be used:

* [preprocess_stitching](https://gitlab.esrf.fr/tomotools/nabu/-/blob/master/doc/Tutorials/preprocess_stitching.ipynb): to ease full preprocessing stitching, display some raw data and some basic stiching
* [postprocess_stitching](https://gitlab.esrf.fr/tomotools/nabu/-/blob/master/doc/Tutorials/postprocess_stitching.ipynb): to ease full postprocessing stitching, display some raw data and some basic stiching
* [overlap_stitcher](https://gitlab.esrf.fr/tomotools/nabu/-/blob/master/doc/Tutorials/overlap_stitcher.ipynb): to display and ease definition of overlap areas and result on a single slice. Here is an example on how the overlap areas will be displayed (colored rectangle of alpha==0.5 are the one that can be used for overlap):

![overlap](images/overlap_areas_from_notebook.png)

### note on calling nabu with the stitched NXtomo

Pre processing z-stitching will create a NXtomo containing projection already flat fielded. This is why **you should turn of `flatfield` field** from the nabu configuration file. as it does not contain any dark or flat it would raise an error otherwise.

### note on ordering

Algorithm expects to have the scan z decreasing ordered (first scan provided should be the upper one).
Only the `ZStitcher` and the nabu-stiching can **invert only** scan z ordering automatically.

It expect to have something like:

![z order](images/z_stitching_ordering.png)

### note on frame flips

The NXtomo can be aware of some frame flipping in x or in y.
Those flips will be handled by the stitching algorithm. But be aware that stitching algorithm will generate unflipped data.
This might be a bit disturbing when comparing stitched data with raw data.

### note on management of rotation angle

some acquisition can have inverted rotation angle. To speed up acquition over several z then user might want to avoid to reset to original position motors.
This case is hanled by hckecing the order as long as rotation angle are coherent.

### real overlaps vs stitching overlap.

Between to overlaping scan or volumes exists a 'real overlap'. Which can be obtain from the real positions of the two objects + object shapes.
Positions are deduced from metada of the object. They can also be redefined by either `axis_X_pos_px` (one per object) or by `axis_X_pos_mm` + pixel size or voxel size.

![real overlap](images/z_stitch_real_overlap.png)

Then the algorithm will do the stitching using a `overlap size` ( <= `real overlap`). Of course this height must be smaller or equal to the real overlap otherwise it cannot process. This size height be defined by the `overlap_size` key from `axis_0_params` parameter.

![stitching height](images/z_stitch_stitch_height.png)

On this example we ask the z-stitching to be 40 px height

```txt
axis_0_params = overlap_size=40
```

## warning / bottlenecks

pre-processing stitching works for NXtomo format. If your entry is an EDF please convert it to NXtomo first. A tutorial can be find [here](https://tomotools.gitlab-pages.esrf.fr/nxtomomill/tutorials/edf2nx.html)

post-processing stitching is meant to work any file format handled by nabu. Nevertheless it first target HDF5. For other file format it is expected to be **unoptimised**. For now for example it will duplicate all slice files to do the concatenation.
