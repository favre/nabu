.. _stitching:

.. |stitching_icon| image:: images/stitching_icon.png
  :width: 40
  :alt: text

Stitching |stitching_icon|
--------------------------

Stitching is the action of aggregating together several projections datasets or several reconstructed volumes.
This is a brief overview of stitching methods and how to benefit from it.

.. toctree::
   :maxdepth: 3

   z-stitching.md
   overlap_kernels.md
   shift_search.md
   alignment.md
   normalization_by_sample.md
   distribution_on_slurm.md
   design.md
   data_duplication.md
