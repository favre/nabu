# normalization by sample

You can ask to apply to each frame line a normalization based on a sample of the frame.
It will compute the mean or the median of each lines of this sample area. And substract for each line of the frame the mean / median of the line of the sample.

## configuration

For example by adding the following line to the stitching configuration file we will request this normalization to be done.

``` text
[normalization_by_sample]
# section dedicated to normalization by a sample. If activate each frame can be normalized by a sample of the frame
# should we apply frame normalization by a sample or not
active = True
side = right
# possible margin before sampling start
margin = 0
# sampling width
width = 50
method = median
```

Here is a drawing of the terminology used - the sample area to use is in red:

![normalization by sample vocabulary](images/normalization_by_sample.svg)
