# alignment

If you have different volume or projection dimension you might need to process to some alignment in order to process stitching.

## alignment on projections

For z-stitching on projections you can align projection along the axis 1

Possible alignments are 'center', 'left' and 'right'

Here are examples of alignment along axis 2.
On the following examples we have two datasets with the same frame width (red) and one dataset with a smaller width (blue)

### 'left' alignment

!['left' alignment along axis 1](images/alignment/proj_alignment_axis_1_left.png)

### 'right' alignment

!['right' alignment along axis 1](images/alignment/proj_alignment_axis_1_right.png)

### 'center' alignment

!['center' alignment along axis 1](images/alignment/proj_alignment_axis_1_center.png)

## alignment on reconstructed volumes

For z-stitching on reconstructed volume you can align projection along the axis 1 and the axis 2.

Possible alignments along axis 2 are the same as for alignment on projections ('center', 'left', 'right').
Along axis 1 valid values are 'center', 'back', 'front'.

Here are examples of alignment along axis 1 and axis 2.
On the following examples we have two volumes with the same shape (in blue) and one 'smaller' volume (in red).

Note: The padded data is not modelize on the drawing to ease undertanding. But you can easily imagine it.

### 'front' alignment along axis 2 (and left along axis 1)

!['front' alignment along axis 2 (and left along axis 1)](images/alignment/alignment_axis_2_front.png)

### 'back' alignment along axis 2 (and left along axis 1)

!['back' alignment along axis 2 (and left along axis 1)](images/alignment/alignment_axis_2_back.png)

### 'center' alignment along axis 2 (and left along axis 1)

!['center' alignment along axis 2 (and left along axis 1)](images/alignment/alignment_axis_2_center_left.png)

### 'center' alignment along axis 2 (and right along axis 1)

!['center' alignment along axis 2 (and right along axis 1)](images/alignment/alignment_axis_2_center_right.png)

### 'center' alignment along axis 2 (and center along axis 1)

!['center' alignment along axis 2 (and center along axis 1)](images/alignment/alignment_axis_2_center_center.png)
