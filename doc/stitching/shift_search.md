# shift algorithms

In order to find shift several methods are available (`img_reg_method` key):

* None: in this case only the motors position / volume position will be used and the pixel size
* `skimage`: use sci-kit image 'phase_cross_correlation' function with a specific space (`real` or `fourier`)
* `nabu-fft`: use nabu `find_shift_correlate` function

Settings of the shift search can be done with the `axis_X_params` keyword from the configuration file.
Synthax is like `key1=value1,key2=value2`. For now valid keys are:
And some parameter can also be specified like to set the window search size with 'window_size'.

* first example: ask shift search over the axis 0 (aka z) with `nabu-fft` on a window of 400 px around the expected overlap.

    ```txt
    axis_0_params = overlap_size=;img_reg_method=nabu-fft;window_size=400
    ```

* second example: ask the shifting algorithm to take the raw value of volume or scan for axis 2 (aka x)

    ```txt
    axis_2_params = img_reg_method=None;
    ```

Finally here is the list of the different keywork the `axis_x_params` can contain:

* img_reg_method: details upper
* overlap_size: size to apply stitching. If not provided will take the largest size possible'.
* low_pass: low pass filter value for filtering frames before shift research
* high_pass: high pass filter value for filtering frames before shift research
