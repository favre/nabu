.. Nabu documentation master file, created by
   sphinx-quickstart on Thu Oct  3 19:50:27 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Nabu |version|
==============

Nabu is a tomography software used at the `European Synchrotron <https://www.esrf.eu>`_.


Quick links:

   * `Nabu development place <https://gitlab.esrf.fr/tomotools/nabu/>`_
   * Project maintainer: `Pierre Paleo <mailto:pierre.paleo@esrf.fr>`_

Latest news:

  * December 2024: Version 2024.2.0 released


Getting started
---------------

.. toctree::
   :maxdepth: 1

   install.md
   quickstart.md
   nabu_cli.md
   nabu_config_file.md
   about.md


Using nabu
-----------

.. toctree::
   :maxdepth: 1

   nabu_config_items.md
   nabu_at_esrf.md
   nabu_advanced_cli.md


Features
--------
.. toctree::
   :maxdepth: 1

   features.md
   phase.md   
   estimation.md
   helical.md	      
   stitching/index.rst


Command-line tools
-------------------

.. toctree::
   :maxdepth: 1

   tests.md

.. toctree::
   :maxdepth: 2

   cli_tools.md


Using nabu from python
----------------------
.. toctree::
   :maxdepth: 1

   Tutorials/nabu_basic_reconstruction
   Tutorials/nabu_from_python_with_gpu
   Tutorials/overlap_kernels
   Tutorials/remote_batch_processing
.. Tutorials/preprocess_stitching
.. Tutorials/postprocess_stitching


Developers
------------

.. toctree::
   :maxdepth: 1

   design.md
   architecture1.md
   configparsing.md
   definitions.md


API reference
--------------

.. toctree::
   :maxdepth: 3

   apidoc/nabu.rst


Release history
----------------

.. toctree::
   :maxdepth: 1


   v2024_2_0.md
   v2024_1_0.md
   v2023_2_0.md
   v2023_1_0.md
   v2022_2_0.md
   v2022_1_0.md
   v2021_2_0.md
   v2021_1_0.md
   v2020_5_0.md
   v2020_4_0.md
   v2020_3_0.md
   v2020_2_0.md



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
