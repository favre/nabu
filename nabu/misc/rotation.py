from ..processing.rotation import *
from ..utils import deprecation_warning

deprecation_warning(
    "nabu.misc.rotation has been moved to nabu.processing.rotation", do_print=True, func_name="rotation"
)
