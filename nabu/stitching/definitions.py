from silx.utils.enum import Enum as _Enum


class StitchingType(_Enum):
    Y_PREPROC = "y-preproc"
    Z_PREPROC = "z-preproc"
    Z_POSTPROC = "z-postproc"
